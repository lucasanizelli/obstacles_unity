﻿using UnityEngine;
using System.Collections;

public class ScriptTest : MonoBehaviour {

    private Animator anim_set;
    private CharacterController controller;
    
    public Transform jumpLocation;
    public bool canJumpHigh = false;
    public bool canJumpLow = false;

    private float VerticalAxis = 0f;

	// Use this for initialization
    void Start()
    {
        anim_set = GetComponent<Animator>();
        controller = GetComponent<CharacterController>();
    }
	
	// Update is called once per frame
    void Update()
    {
        VerticalAxis = Input.GetAxis("Vertical");
        anim_set.SetFloat("Speed", VerticalAxis); //set the spped with the vertical axis

        if (Input.GetKeyDown(KeyCode.Space) && VerticalAxis > .1f) //if space is pressed and it is running
        {
            if (canJumpHigh)
                anim_set.SetBool("CanJump", true); //we can jump
            else if (canJumpLow)
                anim_set.SetBool("CanJumpLow", true);
        }
        else if (VerticalAxis < 0 && controller.isGrounded)
            transform.Translate((Vector3.back / 2) * Time.deltaTime, Space.World);

        Camera.main.transform.position = new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, transform.position.z + 7);

        //Debug.Log("FLOAT HEIGHT:" + anim_set.GetFloat("ControllerHeight"));
        //controller.height = anim_set.GetFloat("ControllerHeight");
    }
    
    /// <summary>
    /// Event called after jump anim is finished
    /// </summary>
    void onJumpEnd()
    {
        //controller.height = 1.6f; //after the jump we restore the collider height
        anim_set.SetBool("CanJump", false); //is not jumping anymore
    }

    /// <summary>
    /// Event called to place the obj in the correct sport to jump
    /// </summary>
    void jumpToLocation()
    {
        controller.height = 0;
        controller.radius = .1f;
        //iTween.MoveTo(jumpLocation.gameObject, iTween.Hash("time", .8f, "easetype", iTween.EaseType.linear));
        moveLinear(gameObject, jumpLocation.position, 2f);
    }

    /// <summary>
    /// Event called after the lands in the smooth zone, restore the radius for proper collision
    /// </summary>
    void onRestoreRadius()
    {
        controller.radius = .4f;
    }

    void onRestoreHeight()
    {
        controller.height = 1.6f; //after the jump we restore the collider height
    }

    void onJumpLowStarted()
    {
        controller.height = 0;
        controller.radius = .2f;
        //iTween.MoveTo(jumpLocation.gameObject,iTween.Hash("time", .8f, "easetype", iTween.EaseType.linear));
        moveLinear(gameObject, jumpLocation.position, 2f);
    }

    void onJumpLowEnd()
    {
        controller.height = 1.6f;
        controller.radius = .4f;
        anim_set.SetBool("CanJumpLow", false);
    }

    /// <summary>
    /// Move one object to goal position without using Update
    /// </summary>
    /// <param name="obj">The object to move</param>
    /// <param name="goal">The position where to move obj</param>
    /// <param name="rateOfMovement">the Speed of the movement</param>
    void moveLinear(GameObject obj, Vector3 goal, float rateOfMovement)
    {
        Vector3 start;
        while (true)
        {
            start = obj.transform.position;
            if (start == goal)
            {
                //UnityEditor.EditorApplication.isPaused = true;
                break;
            }
            obj.transform.position = Vector3.MoveTowards(start, goal, Time.deltaTime * rateOfMovement);
        }
    }

    void OnTriggerEnter(Collider c)
    {
        if (c.name == "obstacleHigh")
        {
            jumpLocation = c.GetComponent<ObstacleHigh>().GetJumpLocation();
            canJumpHigh = true;
        }
        else if (c.name == "obstacleLow")
        {
            jumpLocation = c.GetComponent<ObstacleHigh>().GetJumpLocation();
            canJumpLow = true;
        }
    }

    void OnTriggerExit(Collider c)
    {
        if (c.name == "obstacleHigh")
        {
            canJumpHigh = false;
        }
        else if (c.name == "obstacleLow")
        {
            canJumpLow = false;
        }
    }
}
